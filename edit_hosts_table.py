from app import app, db
from sqlalchemy import Table, MetaData, String, Column, Boolean, Text
from app.data import models

def upgrade(migrate_engine):
        
        city = Column('city', String(20), nullable=False)
        hosted_before = Column(Boolean, nullable=False)
        experience_type = Column(String(50), nullable=False)
        language = Column(String(20), nullable=False)
        experience_title = Column(String(20), nullable=False)
        photo = Column(String(150), nullable=False)
        location = Column(Text, unique=False, nullable=False)
        details = Column(Text, nullable=False)
        provisions = Column(String(50), nullable=False)
        other_info = Column(Text, nullable=False)
        
        meta = MetaData(bind=migrate_engine)
        mytable = Table('hosts', meta, autoload=True)
        city.create(mytable)
        hosted_before.create(mytable)
        experience_type.create(mytable)
        language.create(mytable)
        experience_title.create(mytable)
        photo.create(mytable)
        location.create(mytable)
        details.create(mytable)
        provisions.create(mytable)
        other_info.create(mytable)