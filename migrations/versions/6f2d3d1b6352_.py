"""empty message

Revision ID: 6f2d3d1b6352
Revises: dafb265bdb4c
Create Date: 2018-05-08 12:51:43.086262

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6f2d3d1b6352'
down_revision = 'dafb265bdb4c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('relationship_table_ibfk_1', 'relationship_table', type_='foreignkey')
    op.drop_constraint('relationship_table_ibfk_2', 'relationship_table', type_='foreignkey')
    op.create_foreign_key(None, 'relationship_table', 'ratings', ['rating_id'], ['id'], ondelete='cascade')
    op.create_foreign_key(None, 'relationship_table', 'experiences', ['experience_id'], ['id'], ondelete='cascade')
    op.drop_constraint('user_relationship_table_ibfk_1', 'user_relationship_table', type_='foreignkey')
    op.drop_constraint('user_relationship_table_ibfk_2', 'user_relationship_table', type_='foreignkey')
    op.create_foreign_key(None, 'user_relationship_table', 'users', ['user_id'], ['id'], ondelete='cascade')
    op.create_foreign_key(None, 'user_relationship_table', 'experiences', ['experience_id'], ['id'], ondelete='cascade')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'user_relationship_table', type_='foreignkey')
    op.drop_constraint(None, 'user_relationship_table', type_='foreignkey')
    op.create_foreign_key('user_relationship_table_ibfk_2', 'user_relationship_table', 'users', ['user_id'], ['id'])
    op.create_foreign_key('user_relationship_table_ibfk_1', 'user_relationship_table', 'experiences', ['experience_id'], ['id'])
    op.drop_constraint(None, 'relationship_table', type_='foreignkey')
    op.drop_constraint(None, 'relationship_table', type_='foreignkey')
    op.create_foreign_key('relationship_table_ibfk_2', 'relationship_table', 'ratings', ['rating_id'], ['id'])
    op.create_foreign_key('relationship_table_ibfk_1', 'relationship_table', 'experiences', ['experience_id'], ['id'])
    # ### end Alembic commands ###
