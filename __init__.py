import os
from datetime import datetime

from flask import Flask, request, render_template, Response, url_for
from config import configure_app
from app.utils import get_instance_folder_path
from flask_mail import Mail
from flask_mail import Message
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
from sqlalchemy.sql import func
from flask_login import current_user, LoginManager
from werkzeug.contrib.fixers import ProxyFix
from flask_dance.contrib.google import make_google_blueprint, google
from flask_dance.contrib.facebook import make_facebook_blueprint, facebook
from flask_moment import Moment

from elasticsearch import Elasticsearch
# from flask_emails import Message
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://04c0d6a27e184473a3e913e7f0a41bff@sentry.io/1291155",
    integrations=[FlaskIntegration()]
)


mail = Mail()
db = SQLAlchemy(session_options={"autoflush": False})

app = Flask(__name__, 
	instance_path=get_instance_folder_path(), 
	instance_relative_config=True, template_folder='templates')

configure_app(app)
app.wsgi_app = ProxyFix(app.wsgi_app)
db.init_app(app)
mail.init_app(app)
ma = Marshmallow(app)
login_manager = LoginManager(app)
moment = Moment(app)


app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
        if app.config['ELASTICSEARCH_URL'] else None

# app.config = {'EMAIL_HOST': 'smtp.googlemail.com', 'EMAIL_PORT': 25, 'EMAIL_TIMEOUT': 30}

# logging configs

if not app.debug:
    if app.config['MAIL_SERVER']:
        auth = None
        if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
            auth = (app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
        secure = None
        if app.config['MAIL_USE_TLS']:
            secure = ()
        mail_handler = SMTPHandler(
            mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
            fromaddr='no-reply@' + app.config['MAIL_SERVER'],
            toaddrs=app.config['TUBAYO_ADMIN'], subject='Tubayo Failure',
            credentials=auth, secure=secure)
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

    if not os.path.exists('logs'):
            os.mkdir('logs')
    file_handler = RotatingFileHandler('logs/tubayolog.log', maxBytes=10240,
                                           backupCount=10)
    file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Tubayo startup')


# @app.route('/about')
# def about():
#     app.logger.warning('A warning message is sent.')
#     app.logger.error('An error message is sent.')
#     app.logger.info('Information: 3 + 2 = %d', 5)
#     return "about"

# @app.route("/send")
# def send():
#    msg = Message(subject='Registration',
#                                  body='Thanks for registering with Kennedy Family Recipes!',
#                                  recipients=['quinton.ssebaggala@gmail.com'])
#    mail.send(msg)
#    return "Sent"

@app.route("/send")
def send():
    msg = Message("Hello quinton2",
                      sender="quinton.ssebaggala@gmail",
                      recipients=["ssebaggalaq@gmail.com"])
    mail.send(msg)

    # if r.status_code not in [250, ]:
    #     # message is not sent, deal with this
    #     pass
    return "Sent"

# error handling

# @app.errorhandler(404)
# def page_not_found(error):
#     title='404'
#     app.logger.error('Page not found: %s', (request.path))
#     return render_template('errors/404.html', title=title), 404

# @app.errorhandler(500)
# def internal_server_error(error):
#     title='500'
#     app.logger.error('Server Error: %s', (error))
#     return render_template('errors/500.html', title=title), 500

# # @app.errorhandler(403)
# # def internal_server_error(error):
# #     app.logger.error('Server Error: %s', (error))
# #     return render_template('errors/500.html'), 403

# # @app.errorhandler(410)
# # def internal_server_error(error):
# #     app.logger.error('Server Error: %s', (error))
# #     return render_template('errors/500.html'), 410

# @app.errorhandler(Exception)
# def unhandled_exception(e):
#     title='500'
#     app.logger.error('Unhandled Exception: %s', (e))
#     return render_template('errors/500.html', title=title), 500


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    app.jinja_env.cache = {}



blueprint = make_google_blueprint(
    client_id=os.environ.get('google_client_id'),
    client_secret=os.environ.get('google_client_secret'),
    scope=["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"],
    redirect_url='https://trevornagaba.pythonanywhere.com/experience'
)

facebook_blueprint = make_facebook_blueprint(
    client_id=os.environ.get('facebook_client_id'),
    client_secret=os.environ.get('facebook_client_secret'),
    scope=['public_profile','user_friends','email'],
    redirect_url='https://trevornagaba.pythonanywhere.com/experience'    
 )

#flask register_blueprint()

from app.main import main
from app.admin import admin
from app.admin import in_line
# from app.admin import admin
from app.auth import auth
# from social_flask.routes import social_auth
from app.API.api import stories

app.register_blueprint(stories, url_prefix='/api/v1')

app.register_blueprint(main)
app.register_blueprint(auth, url_prefix='/auth')
app.register_blueprint(blueprint, url_prefix="/login")
app.register_blueprint(facebook_blueprint, url_prefix="/login") 

#redirect views
login_manager.login_view = 'auth.login'


if __name__ == '__main__':
    app.run()