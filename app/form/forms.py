from flask_wtf.file import FileField, FileRequired, FileAllowed

from app.data.models import Advert, ShoppingCart, Date_available, Experience, Flyer, Image, Imageexp, Rating, Shop, Slideshow, Story, User, Hosty, Book
from sqlalchemy import TIMESTAMP, Column, Integer, Time, DateTime, Float, Text, ForeignKey, PrimaryKeyConstraint, String, Unicode, Date, Boolean, Enum, Numeric, Table, MetaData
from wtforms.fields import FormField, SelectField
from wtforms_alchemy import ModelFieldList
from wtforms import DateField, PasswordField, BooleanField, SubmitField, HiddenField, TextAreaField
from wtforms.validators import DataRequired, EqualTo, Email, ValidationError, Length
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
 ####
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_wtf import FlaskForm
from wtforms_alchemy import model_form_factory
from wtforms_alchemy.fields import StringField, ModelFormField
from app.data.models import User
from app import db
from flask_wtf import Form
from wtforms import SubmitField, SelectField, DateField
from flask import Flask, render_template, request
from datetime import datetime
from flask_admin.form.widgets import DatePickerWidget
from urllib.parse import urlparse, urljoin #urllib.parse
from flask import request, url_for, redirect
import re
# from flask_babel import lazy_gettext as _l


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


def get_redirect_target():
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target

class RedirectForm(FlaskForm):
    next = HiddenField()

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ''

    def redirect(self, endpoint='index', **values):
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        return redirect(target or url_for(endpoint, **values))

# Using WTForms-Alchemy with Flask-WTF (http://wtforms-alchemy.readthedocs.org/en/latest/advanced.html#using-wtforms-alchemy-with-flask-wtf) to include all good features of Flask-WTF https://flask-wtf.readthedocs.org/en/latest/#features
BaseModelForm = model_form_factory(FlaskForm)


class ModelForm(BaseModelForm):
    @classmethod
    def get_session(self):
        return db.session


class AdvertForm(ModelForm):
    class Meta:
        model = Advert

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])


class CartForm(ModelForm):
    class Meta:
        model = ShoppingCart


class Date_availableForm(ModelForm):
    class Meta:
        model = Date_available


class FlyerForm(ModelForm):
    class Meta:
        model = Flyer

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])



class ImageForm(ModelForm):
    class Meta:
        model = Image

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])



class ShopForm(ModelForm):
    class Meta:
        model = Shop

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])

class SlideshowForm(ModelForm):
    class Meta:
        model = Slideshow

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])

class StoryForm(ModelForm):
    class Meta:
        model = Story
        # exclude = ['email']

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])


class ExperienceForm(ModelForm):
    experience_title = StringField('Username', validators=[DataRequired()])
    # tag = Column(Text, nullable=False)
    # email = StringField('Email', validators=[DataRequired()])
    # phone_number = Columnn(String(50), nullable=False)
    # country = StringField('Username', validators=[DataRequired()])
    # place = StringField('Username', validators=[DataRequired()])
    # about_experience = Column(Text, nullable=False)
    # what_well_do = Column(Text, nullable=False, info={'label': 'What we will do'})
    # who_can_come = Column(Text, nullable=False)
    # price = Column(Float(20), nullable=False)

    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])
    # dates = DateField(label='Date', format='%Y-%m-%d')
    # image = ModelFieldList(FormField(ImageForm))
    # cart = ModelFieldList(FormField(CartForm))
    # date = ModelFieldList(FormField(Date_available))

class HostForm(ModelForm):
    # class Meta:
    #    model = Host
    #hosts = ModelFieldList(FormField(ExperienceForm))
    host_name = StringField('Host Name', validators=[DataRequired()])
    about_host = TextAreaField('About Host', validators=[DataRequired()])
    citys = StringField('City', validators = [DataRequired()])#, choices=[('Kampala', 'Kampala'), ('Nairobi', 'Nairobi'), ('Jinja', 'Jinja'), ('Mbarara', 'Mbarara'), ('Kigali', 'Kigali'), ('Mombasa', 'Mombasa'),('Khartoum', 'Khartoum'), ('Naivasha', 'Naivasha'), ('Entebbe', 'Entebbe')])
    
    hosted_before = SelectField('Have you hosted before?', validators = [DataRequired()], choices=[('No', 'No'), ('Yes', 'Yes')])
    experience_type = SelectField('What type of experience will you host?', validators = [DataRequired()], choices=[('Wildlife', 'Wildlife'), ('Safaris', 'Safaris'), ('Camping', 'Camping'), ('Hiking', 'Hiking'), ('City Tours', 'City Tours'), ('Lifestyle', 'Lifestyle'), ('Arts & Design', 'Arts & Design'), ('Sports', 'Sports'), ('History', 'History'), ('Entertainment', 'Entertainment'), ('Business', 'Business'), ('Night Life', 'Night Life'),])
    language = SelectField('What language will you host in?', validators = [DataRequired()], choices=[('English', 'English'), ('Chinese', 'Chinese'), ('French', 'French'), ('Spanish', 'Spanish'), ('German', 'German')])
    experience_title = StringField('Desribe your experience in 5 words or less?', validators = [DataRequired()])
    filename = FileField(None, [
        FileRequired(),
        FileAllowed(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'], 'Image Only!')
    ])
    location = TextAreaField('Where should guests find you? Give your street address followed by directions on a new line', validators = [DataRequired()])
    details = TextAreaField('Give a more detailed and fun description of your experience. What will you actually do?', validators = [DataRequired()])
    provisions = SelectField('What will you provide?', validators = [DataRequired()], choices=[('Meals', 'Meals'), ('Drinks', 'Drinks'), ('Accomodation', 'Accomodation'), ('Tickets', 'Tickets'), ('Transportation', 'Transportation'), ('Equipment', 'Equipment'), ('Snacks', 'Snacks')])
    other_info = TextAreaField('What else do guests need to know?', validators=[DataRequired()])
#############################
class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()], render_kw={'aria-label': "Search, discover, explore...", 'aria-describedby':"basic-addon2" })

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
################################


################################
class LoginForm(RedirectForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    # submit = SubmitField('Sign In')

    def validate_login(self, field):
        user = self.get_user()
        return back.redirect()
        if user is None:
            raise validators.ValidationError('Invalid user')

        # we're comparing the plaintext pw with the the hash from the db
        if not check_password_hash(user.password_hash, self.password.data):
        # to compare plain text passwords use
        # if user.password != self.password.data:
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(User).filter_by(email=self.email.data).first()

class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')


def length_check(form, field):
    while True:
        if 8 <= len(field.data) < 100:
            break
        raise ValidationError('The password must be atleast 8 characters')
'''
    password_scores = {0:'Horrible', 1:'Weak', 2:'fair', 3:'Strong'}
    password_strength = dict.fromkeys(['has_upper', 'has_lower', 'has_num'], False)
    if re.search(r'[A-Z]', field.data):
        password_strength['has_upper'] = True
    if re.search(r'[a-z]', field.data):
        password_strength['has_lower'] = True
    if re.search(r'[0-6]', field.data):
        password_strength['has_num'] = True

    score = len([b for b in password_strength.values() if b])

    if password_scores[score] == 'Weak' or password_scores[score] == 'fair':
        raise ValidationError('Password is %s' % password_scores[score])
'''
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    phone = StringField('Phone', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), length_check])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class ContactForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    subject = StringField('Subject', validators=[DataRequired()])
    body = TextAreaField('Body', validators=[DataRequired(), Length(max=2047)], render_kw={'rows': 6})
    # render_kw={'class': 'form-control', 'rows': 5}

class CommentForm(FlaskForm):
    comment = TextAreaField('Comment', validators=[DataRequired(), Length(max=2047)], render_kw={'rows': 6})

class BookForm(ModelForm):
    # class Meta:
    #    model = Host
    #hosts = ModelFieldList(FormField(ExperienceForm))
    
    experience_title = TextField('.', render_kw={"placeholder": "Enter Experience id above."})
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    datey = StringField('Date of Birth', validators=[DataRequired()], render_kw={"placeholder": "year-month-day"})
    phone = StringField('Phone', validators=[DataRequired()])
    country = SelectField('Country', validators = [DataRequired()], choices=[('Uganda', 'Uganda'), ('Kenya', 'Kenya'), ('Rwanda', 'Rwanda'), ('Tanzania', 'Tanzania'), ('Congo', 'Congo'), ('Burundi', 'Burundi')])
    accomodation = SelectField('Do you need accomodation?', validators = [DataRequired()], choices=[('No', 'No'), ('Yes', 'Yes')])
    date = StringField('Check in Date', validators=[DataRequired()], render_kw={"placeholder": "year-month-day"})
    details = TextAreaField('Give a more detailed and fun description of your experience. What will you actually do?', validators = [DataRequired()])
    guests = SelectField('Number of Guests?', validators = [DataRequired()], choices=[('One', 'One'), ('Two', 'Two'), ('Three', 'Three'), ('Four', 'Four'), ('Five or more', 'Five or more')])
    other_info = TextAreaField('Any other special requests?', validators=[DataRequired()])
  