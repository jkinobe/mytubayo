from flask import Flask
from flask_sqlalchemy import SQLAlchemy 
from flask_appbuilder import Model
from flask_appbuilder.models.mixins import ImageColumn
from flask_login import UserMixin, AnonymousUserMixin, current_user
from app import login_manager, app, db, blueprint, facebook_blueprint, ma
from sqlalchemy.sql import func
from sqlalchemy import TIMESTAMP, Column, Integer, Time, DateTime, Float, Text, ForeignKey, PrimaryKeyConstraint, String, Unicode, Date, Boolean, Enum, Numeric, Table, MetaData
from sqlalchemy.orm import relationship, backref, composite
from sqlalchemy_utils import aggregated, CountryType, EmailType, PhoneNumber
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import exc
from wtforms.validators import Email
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms import validators
from wtforms.validators import InputRequired
from wtforms.validators import DataRequired
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin, SQLAlchemyBackend
import jwt
from datetime import datetime
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
# marshmallow
from app import db
from marshmallow import Schema, fields

from sqlalchemy.types import TypeDecorator
from sqlalchemy.dialects.mysql import DOUBLE, TIME
from datetime import datetime
from hashlib import md5
from time import time

from app.search import add_to_index, remove_from_index, query_index

class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)).order_by(
            db.case(when, value=cls.id)), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': [obj for obj in session.new if isinstance(obj, cls)],
            'update': [obj for obj in session.dirty if isinstance(obj, cls)],
            'delete': [obj for obj in session.deleted if isinstance(obj, cls)]
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            add_to_index(cls.__tablename__, obj)
        for obj in session._changes['update']:
            add_to_index(cls.__tablename__, obj)
        for obj in session._changes['delete']:
            remove_from_index(cls.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)

db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)



class DoubleTimestamp(TypeDecorator):
    impl = DOUBLE

    def __init__(self):
        TypeDecorator.__init__(self, as_decimal=False)

    def process_bind_param(self, value, dialect):
        return value.replace(tzinfo=datetime.timezone.utc).timestamp() * 1000

    def process_result_value(self, value, dialect):
        return datetime.datetime.utcfromtimestamp(value / 1000)


class CRUD():   

    def add(self, resource):
        db.session.add(resource)
        return db.session.commit()   

    def update(self):
        return db.session.commit()

    def delete(self, resource):
        db.session.delete(resource)
        return db.session.commit()


class Base(db.Model, CRUD):
    __abstract__ = True

    created_on = Column(DateTime, default=datetime.utcnow)
    updated_on = Column(DateTime(timezone=True),
                        default=func.now(), onupdate=datetime.utcnow)


class Permission:
    FOLLOW = 1
    COMMENT = 2
    WRITE = 4
    MODERATE = 8
    ADMIN = 16


user_relationship_table=db.Table('user_relationship_table', 
                             Column('experience_id', Integer,ForeignKey('experiences.id', ondelete="cascade"), nullable=False),
                             Column('user_id',Integer,ForeignKey('users.id', ondelete="cascade"),nullable=False),
                             PrimaryKeyConstraint('experience_id', 'user_id') )


class User(UserMixin, Base):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = Column(String(64), index=True, unique=False)
    email = Column(String(120), index=True, unique=True)
    phone_number = Column(String(50), nullable=True)
    password_hash = Column(db.String(128))
    avator = Column(String(200))
    last_seen = Column(DateTime(timezone=True), default=func.now())
    role_id = Column(Integer, ForeignKey('roles.id'))
    comment = relationship('Comment', backref='comments', cascade="all, delete-orphan", lazy='dynamic')

    carts = relationship('ShoppingCart', backref='user_cart',
                         cascade="all, delete-orphan", lazy='dynamic')

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.email == app.config['TUBAYO_ADMIN'][0]:
            self.role = Role.query.filter_by(name='Administrator').first()
        if self.role is None:
            self.role = Role.query.filter_by(default=True).first()

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    def is_administrator(self):
        return self.can(Permission.ADMIN)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


    # Required for administrative interface
    def __unicode__(self):
        return self.username 

# @login_manager.user_loader
# def load_user(id):
#     return User.query.get(int(id))


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


class AnonymousUser(AnonymousUserMixin):

    def can(self, permissions):
        return False

    def is_administrator(self):
        return False


login_manager.anonymous_user = AnonymousUser

class OAuth(OAuthConsumerMixin, db.Model):
    provider_user_id = Column(String(128), unique=True)
    user_id = Column(Integer, ForeignKey(User.id))
    user = relationship(User)


# setup SQLAlchemy backend
blueprint.backend = SQLAlchemyBackend(OAuth, db.session, user=current_user)
facebook_blueprint.backend = SQLAlchemyBackend(OAuth, db.session, user=current_user)


class Role(db.Model):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)
    default = Column(Boolean, default=False, index=True)
    permissions = Column(Integer)
    users = relationship('User', backref='role', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)

        if self.permissions is None:
            self.permissions = 0

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    @staticmethod
    def insert_roles():
        roles = {
                    'User': [Permission.FOLLOW, Permission.COMMENT, Permission.WRITE],
                    'Moderator': [Permission.FOLLOW, Permission.COMMENT,
                    Permission.WRITE, Permission.MODERATE],
                    'Administrator': [Permission.FOLLOW, Permission.COMMENT,
                    Permission.WRITE, Permission.MODERATE,
                    Permission.ADMIN],
                }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()

    def __repr__(self):
        return '<Role - {}>'.format(self.name)



relationship_table=db.Table('relationship_table', 
                             Column('experience_id', Integer,ForeignKey('experiences.id', ondelete="cascade"), nullable=False),
                             Column('rating_id',Integer,ForeignKey('ratings.id', ondelete="cascade"),nullable=False),
                             PrimaryKeyConstraint('experience_id', 'rating_id') )

category_experience=db.Table('category_experience', 
                             Column('experience_id', Integer,ForeignKey('experiences.id', ondelete="cascade"), nullable=False),
                             Column('category_id',Integer,ForeignKey('categories.id', ondelete="cascade"),nullable=False),
                             PrimaryKeyConstraint('experience_id', 'category_id') )


class Experience(Base):
    __tablename__ = 'experiences'
    __searchable__ = ['place', 'experience_title']

    id = Column(Integer, primary_key=True)
    experience_title = Column(String(100), unique=True, nullable=False)
    tag = Column(Text, nullable=False)
    email = Column(String(120), nullable=False, 
                    info={'validators': Email()})
    phone_number = Column(String(50), nullable=False)
    country = Column(String(100), nullable=False)
    place = Column(String(50), nullable=False)
    about_experience = Column(Text, nullable=False)
    what_well_do = Column(Text, nullable=False, info={'label': 'What we will do'})
    who_can_come = Column(Text, nullable=False)
    price = Column(Float(20), nullable=False)
    Weighted_Average = Column(Float(20), nullable=True, default=0.1)
    reviews_no = Column(Integer)
    # user_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'))
    host_id = Column(Integer, ForeignKey('hosts.id', ondelete='CASCADE'))

    category_id = Column(Integer, ForeignKey('categories.id', ondelete='CASCADE'))
    category = relationship('Category',
                                   backref=backref('product', lazy='dynamic'))

    rates=relationship('Rating', secondary=relationship_table, backref=backref('rates',
                                                    lazy='dynamic') ) 
    users=relationship('User', secondary=user_relationship_table, backref=backref('users',
                                                    lazy='dynamic') ) 
    categories = relationship('Category', secondary=category_experience,
                                 backref=backref('categories',
                                                    lazy='dynamic'))
    exp_comment = relationship('Comment', backref='exp_comments', cascade="all, delete-orphan", lazy='dynamic')

    # @aggregated('rates', Column(Float, default=0.0))
    # def rate_average(self):
    #     return func.avg(Rate.rating)

    @aggregated('carts', Column(Float, default=0.0))
    def total(self):
        return func.avg(ShoppingCart.total)

    images = relationship(
        'Image', cascade="all, delete-orphan", backref='image', lazy='dynamic')
    imageexp = relationship(
        'Imageexp', cascade="all, delete-orphan", backref='experience', lazy='dynamic')
    carts = relationship('ShoppingCart', backref='cart',
                         cascade="all, delete-orphan", lazy='dynamic')
    dates = relationship('Date_available', backref='date',
                         cascade="all, delete-orphan", lazy='dynamic')
    # rating = relationship('Rating', backref='rate',
    #                      cascade="all, delete-orphan", lazy='dynamic')


    def __init__(self, experience_title=None, tag=None, email=None, phone_number=None, country=None, place=None, about_experience=None, what_well_do=None, who_can_come=None, price=None, images=None, karts=None, dates=None):
        self.experience_title = experience_title
        self.tag = tag
        self.email = email
        self.phone_number = phone_number
        self.place = place
        self.country = country
        self.about_experience = about_experience
        self.what_well_do = what_well_do
        self.who_can_come = who_can_come
        self.price = price

    @staticmethod
    def generate_fake(count=100):
        from sqlalchemy.exc import IntegrityError
        from random import seed
        import forgery_py

        seed()
        for i in range(count):
            p = Experience(
                            experience_title=forgery_py.lorem_ipsum.title(), 
                            tag=forgery_py.lorem_ipsum.sentence(), 
                            email=forgery_py.email.address(), 
                            phone_number=forgery_py.address.phone(),
                            country=forgery_py.address.country(), 
                            place=forgery_py.address.citys(), 
                            about_experience=forgery_py.lorem_ipsum.paragraph(), 
                            what_well_do=forgery_py.lorem_ipsum.sentence(), 
                            who_can_come=forgery_py.lorem_ipsum.sentence(), 
                            price=forgery_py.monetary.formatted_money())
            db.session.add(p)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '<Experience - {}>'.format(self.experience_title)

# db.event.listen(db.session, 'before_commit', Experience.before_commit)
# db.event.listen(db.session, 'after_commit', Experience.after_commit)


class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    category_name = Column(String(20))
    experiences = relationship('Experience', secondary=category_experience,
                               backref=backref('experiences', lazy='dynamic'))

    def __init__(self, **kwargs):
        super(Category, self).__init__(**kwargs)

    def __repr__(self):
        return self.category_name


class Hosty(Base):
    """docstring for Host"""
    __tablename__ = 'hosts'
    id = Column(Integer, primary_key=True)
    host_name = Column(String(50), unique=True, nullable=False)
    about_host = Column(Text, nullable=False)
    #city = Column(String(120), nullable=False)
    citys = Column(String(50), nullable=False)
    hosted_before = Column(String(20), nullable=False)
    experience_type = Column(String(50), nullable=False)
    language = Column(String(20), nullable=False)
    experience_title = Column(String(20), nullable=False)
    filename = Column(String(150), nullable=False)
    location = Column(Text, nullable=False)
    details = Column(Text, nullable=False)
    provisions = Column(String(150), nullable=False)
    other_info = Column(Text, nullable=False)
    experiences = relationship(
        'Experience', cascade="all, delete-orphan", backref='hosts', lazy='dynamic')

    def __repr__(self):
        return '<Hosty - {}>'.format(self.host_name)

class Comment(Base):
    """docstring for Host"""
    __tablename__ = 'comments'
    id = Column(Integer, primary_key=True)
    comment = Column(String(500), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'))
    experience_id = Column(Integer, ForeignKey('experiences.id'))

    def __repr__(self):
        return '<Comment - {}>'.format(self.comment)

class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150))
    #filenamey = Column(String(150))
    imageexp = relationship(
        'Imageexp', cascade="all, delete-orphan", backref='image', lazy='dynamic')
    experience_id = Column(Integer, ForeignKey('experiences.id', ondelete='CASCADE'))

    def __init__(self, filename=None, experience_id=None):
        self.filename = filename
        #self.filenamey = filenamey
        self.experience_id = experience_id

    def __repr__(self):
        return '<Image - {}>'.format(self.filename)

class Imageexp(Base):
    id = Column(Integer, primary_key=True)
    image_id = Column(Integer, ForeignKey('images.id'))
    experience_id = Column(Integer, ForeignKey('experiences.id'))
    trend = Column(Boolean, default=False)
    no_people = Column(Integer, default=1)

    def __init__(self, image_id=None, experience_id=None):
        self.image_id = image_id
        self.experience_id = experience_id

    def __repr__(self):
        return '<Imageexp - {}>'.format(self.id)


class Flyer(Base):
    __tablename__ = 'flyers'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150), nullable=False)

    def __init__(self, **kwargs):
        super(Flyer, self).__init__(**kwargs)

    def __repr__(self):
        return '<Flyer - {}>'.format(self.filename)


class Shop( Base):
    __tablename__ = 'shop'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150), nullable=False)
    item_name = Column(String(30), nullable=False)
    price = Column(Float(10), nullable=False)
    description = Column(String(30))


    def __init__(self, filename=None, item_name=None, price=None):
        self.filename = filename
        self.item_name = item_name
        self.price = price

    def __repr__(self):
        return '<Shop - {}>'.format(self.item_name)



class Advert(Base):
    __tablename__ = 'adverts'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150), nullable=False)

    def __init__(self, **kwargs):
        super(Advert, self).__init__(**kwargs)

    def __repr__(self):
        return '<Advert - {}>'.format(self.filename)


class ShoppingCart(Base):
    __tablename__ = 'ShoppingCart'

    id = Column(Integer, primary_key=True)
    item_name = Column(String(100))
    price = Column(Float)
    quantity = Column(Integer)
    total = Column(Float)
    description = Column(Text)
    filename = Column(String(150))
    experience_id = Column(Integer, ForeignKey('experiences.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    date = Column(DateTime)


    # def __init__(self, **kwargs):
    #     super(Role, self).__init__(**kwargs)
    def __init__(self, item_name=None, price=None, quantity=None, total=None, description=description, filename=None, date=None):
        self.item_name = item_name
        self.price = price
        self.quantity = quantity
        self.total = total
        self.description = description
        self.filename = filename
        self.date = date


    def __repr__(self):
        return '<ShoppingCart - {}>'.format(self.item_name)
    # def __repr__(self):
    #     return '<ShoppingCart %r>' % self.username


class Date_available(Base):
    id = Column(Integer, primary_key=True)
    dates_available = Column(DateTime)
    start = Column(Time, nullable=False)
    end = Column(Time, nullable=False)
    experience_id = Column(Integer, ForeignKey(
        'experiences.id', ondelete='CASCADE'))

    def __init__(self, dates_available=None, exp_id=None):
        self.dates_available = dates_available
        self.exp_id = exp_id

    def __repr__(self):
        return '<Date_available - {}>'.format(self.dates_available)


review_relationship_table=db.Table('review_relationship_table', 
                             Column('review_id', Integer,ForeignKey('reviews.id', ondelete="cascade"), nullable=False),
                             Column('rating_id',Integer,ForeignKey('ratings.id', ondelete="cascade"),nullable=False),
                             PrimaryKeyConstraint('review_id', 'rating_id') )

class Rating(Base):
    __tablename__ = 'ratings'

    id = Column(Integer, primary_key=True)
    rating = Column(Integer)

    reviews=relationship('Review', secondary=review_relationship_table, backref='reviews' ) 

    def __init__(self, rating=None, user_id=None, exp_id=None, reviews=None):
        self.rating = rating
        # self.reviews = reviews

    def __repr__(self):
        return '<Rating - {}>'.format(self.rating)


class Review(Base):
    __tablename__ = 'reviews'

    id = Column(Integer, primary_key=True)
    exp_id = Column(Integer)
    rate = Column(Integer)
    review_num = Column(Integer)

    ratings=relationship('Rating', secondary=review_relationship_table, backref='ratings' ) 

    def __init__(self, exp_id=None, rate=None, review_num=None):
        self.exp_id = exp_id
        self.rate = rate
        self.review_num = review_num

    def __repr__(self):
        return '<Review - {}>'.format(self.id)



class Slideshow(Base):
    __tablename__ = 'slideshow'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150), nullable=False)

    def __init__(self, **kwargs):
        super(Slideshow, self).__init__(**kwargs)

    def __repr__(self):
        return '<Slideshow - {}>'.format(self.filename)


class SlideshowMobile(Base):
    __tablename__ = 'slideshowmobile'

    id = Column(Integer, primary_key=True)
    filename = Column(String(150), nullable=False)

    def __init__(self, **kwargs):
        super(SlideshowMobile, self).__init__(**kwargs)

    def __repr__(self):
        return '<SlideshowMobile - {}>'.format(self.filename)



class Story(db.Model):
    __tablename__ = "stories"
    id = Column(Integer, primary_key=True)
    photo = Column(String(150), nullable=False)
    name = Column(String(150), nullable=False)
    link = Column(String(200))
    # lastUpdated = Column(DOUBLE(asdecimal=False))
    lastUpdated = Column(TIME)

    items = relationship('Item', backref='story', lazy='dynamic')
    
    def __init__(self, **kwargs):
        super(Story, self).__init__(**kwargs)

    def __repr__(self):
        return '<Story %r>' % self.name

class Item(db.Model):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True) 
    type = Column(String(200))
    length = Column(Integer, default=6)
    src = Column(String(200))
    preview = Column(String(200))
    link = Column(String(200))
    linkText = Column(Boolean, default=False)
    seen = Column(Boolean, default=False)
    time = Column(Float, default=time())
    # time = Column(TIMESTAMP, default=func.current_timestamp())
    # time = Column(TIMESTAMP, default=datetime.utcnow)Numeric

    story_id = Column(Integer, ForeignKey('stories.id'))
    # story = relationship('Story', backref=backref("items", lazy="dynamic"))
    

    def __init__(self, **kwargs):
        super(Item, self).__init__(**kwargs)

    def __repr__(self):
        return '<Item %r>' % self.id


##### SCHEMAS #####


class ItemSchema(ma.ModelSchema):
    # id = fields.Str(dump_only=True)
    # type = fields.Str()
    # length = fields.Int()
    # src = fields.Str()
    # preview = fields.Str()
    # link = fields.Str()
    # linkText = fields.Boolean()
    # seen = fields.Boolean()
    # time = fields.DateTime(dump_only=True)
    class Meta:
           model = Item

class StorySchema(ma.ModelSchema):
    # id = fields.Str(dump_only=True)
    # photo = fields.Str()
    # name = fields.Str()
    # link = fields.Str()
    # lastUpdated = fields.DateTime(dump_only=True)
    # items = fields.Nested(ItemSchema, many=True)
    class Meta:
            model = Story
    items = ma.Nested(ItemSchema, many=True)
class Book(Base):
    """docstring for Host"""
    __tablename__ = 'book'
    id = Column(Integer, primary_key=True)
    experience_title = Column(String(50), nullable=False)
    first_name = Column(Text, nullable=False)
    last_name = Column(Text, nullable=False)
    email = Column(Text, nullable=False)
    datey = Column(String(20), nullable=False)
    phone = Column(String(50), nullable=False)
    country = Column(String(25), nullable=False)
    accomodation = Column(String(20), nullable=False)
    
    date = Column(String(20), nullable=False)

    details = Column(Text, nullable=False)
    guests = Column(String(50), nullable=False)
    other_info = Column(Text, nullable=False)
    
    def __repr__(self):
        return '<Book - {}>'.format(self.first_name)
