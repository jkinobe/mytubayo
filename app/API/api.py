from flask import Blueprint, request, jsonify, redirect, url_for
from app.data.models import  Story, Item, StorySchema, ItemSchema
from app import db, app
from flask_restful import Api, Resource

 
from sqlalchemy.exc import SQLAlchemyError
from marshmallow import ValidationError

stories = Blueprint('stories', __name__)


# Users
story_schema = StorySchema()
stories_schema = StorySchema(many=True)
item_schema = ItemSchema()
items_schema = ItemSchema(many=True)

@stories.route('/stories')
def get_stories():
    stories = Story.query.all()
    # Serialize the queryset
    result = stories_schema.dump(stories)
    return jsonify({'stories': result.data})

@stories.route("/stories/<pk>")
def get_story(pk):
    try:
        story = Story.query.get(pk)
    except IntegrityError:
        return jsonify({"message": "Author could not be found."}), 400
    story_result = story_schema.dump(story)
    # quotes_result = items_schema.dump(story.items.all())
    return jsonify({'story': story_result.data})

