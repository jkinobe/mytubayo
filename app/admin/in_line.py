import os
import os.path as op

from werkzeug import secure_filename
from sqlalchemy import event

from flask import Flask, request, render_template
from app import db, app

from wtforms import fields

import app.admin.admin as admin
from flask_admin.form import RenderTemplateWidget
from flask_admin.model.form import InlineFormAdmin
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.form import InlineModelConverter
from flask_admin.contrib.sqla.fields import InlineModelFormList
from app.data.models import Experience, Image, Imageexp


# Figure out base upload path
base_path = app.config['EXPERIENCES']
try:
    os.mkdir(base_path)
except OSError:
    pass



# Register after_delete handler which will delete image file after model gets deleted
@event.listens_for(Image, 'after_delete')
def _handle_image_delete(mapper, conn, target):
    try:
        if target.filename:
            os.remove(op.join(base_path, target.filename))
    except:
        pass


# This widget uses custom template for inline field list
class CustomInlineFieldListWidget(RenderTemplateWidget):
    def __init__(self):
        super(CustomInlineFieldListWidget, self).__init__('field_list.html')


# This InlineModelFormList will use our custom widget and hide row controls
class CustomInlineModelFormList(InlineModelFormList):
    widget = CustomInlineFieldListWidget()

    def display_row_controls(self, field):
        return False


# Create custom InlineModelConverter and tell it to use our InlineModelFormList
class CustomInlineModelConverter(InlineModelConverter):
    inline_field_list_type = CustomInlineModelFormList


# Customized inline form handler
class InlineModelForm(InlineFormAdmin):
    form_excluded_columns = ('filename',)

    form_label = 'Image'

    def __init__(self):
        return super(InlineModelForm, self).__init__(Image)

    def postprocess_form(self, form_class):
        form_class.upload = fields.FileField('Image')
        return form_class

    def on_model_change(self, form, model):
        file_data = request.files.get(form.upload.name)

        if file_data:
            model.filename = secure_filename(file_data.filename)
            file_data.save(op.join(base_path, model.filename))


# Administrative class
class LocationAdmin(ModelView):
    form_columns = ['hosts','experience_title', 'tag', 'email', 'phone_number', 'country', 'place', 'about_experience', 'what_well_do', 'who_can_come', 'price', 'dates']
    inline_model_form_converter = CustomInlineModelConverter

    inline_models = (InlineModelForm(),)

    def __init__(self):
        super(LocationAdmin, self).__init__(Experience, db.session, name='Experiences')




# Add views
admin.add_view(LocationAdmin())