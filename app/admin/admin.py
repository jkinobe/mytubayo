import os
import os.path as op

from app import db, app

from app.data.models import Hosty, Flyer, Shop, Slideshow, Book, SlideshowMobile, Imageexp, Date_available, Advert, ShoppingCart, Rating, Review, Story, Item, User, Role, Comment

from flask import url_for, render_template, redirect, request
from app.form.forms import LoginForm, RegistrationForm

from sqlalchemy.event import listens_for
from jinja2 import Markup

from flask_admin import Admin, form
from flask_admin.form import rules
#from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla import ModelView
from flask import session, redirect, url_for, request

import flask_admin as admin
import flask_login as login
from flask_admin import helpers, expose
import flask_login as login
from app.decorators import admin_required, permission_required
from flask_security.decorators import roles_required
# Create directory for file fields to use
# file_path = op.join(op.dirname(__file__), 'static/files')
slide_path = app.config['SLIDES']
try:
    os.mkdir(slide_path)
except OSError:
    pass

story_path = app.config['STORIES']
try:
    os.mkdir(story_path)
except OSError:
    pass

experience_path = app.config['EXPERIENCES']
try:
    os.mkdir(experience_path)
except OSError:
    pass

shop_path = app.config['SHOP']
try:
    os.mkdir(shop_path)
except OSError:
    pass

advert_path = app.config['ADVERTS']
try:
    os.mkdir(advert_path)
except OSError:
    pass

flyer_path = app.config['FLYERS']
try:
    os.mkdir(flyer_path)
except OSError:
    pass


@listens_for(Flyer, 'after_delete')
def del_image(mapper, connection, target):
    if target.filename:
        # Delete image
        try:
            os.remove(op.join(flyer_path, target.filename))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(flyer_path,
                              form.thumbgen_filename(target.filename)))
        except OSError:
            pass

@listens_for(Shop, 'after_delete')
def del_image(mapper, connection, target):
    if target.filename:
        # Delete image
        try:
            os.remove(op.join(shop_path, target.filename))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(shop_path,
                              form.thumbgen_filename(target.filename)))
        except OSError:
            pass


@listens_for(Slideshow, 'after_delete')
def del_image(mapper, connection, target):
    if target.filename:
        # Delete image
        try:
            os.remove(op.join(slide_path, target.filename))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(slide_path,
                              form.thumbgen_filename(target.filename)))
        except OSError:
            pass

@listens_for(Advert, 'after_delete')
def del_image(mapper, connection, target):
    if target.filename:
        # Delete image
        try:
            os.remove(op.join(advert_path, target.filename))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(advert_path,
                              form.thumbgen_filename(target.filename)))
        except OSError:
            pass

@listens_for(Story, 'after_delete')
def del_image(mapper, connection, target):
    if target.photo:
        # Delete image
        try:
            os.remove(op.join(story_path, target.photo))
        except OSError:
            pass

        # Delete thumbnail
        try:
            os.remove(op.join(story_path,
                              form.thumbgen_filename(target.photo)))
        except OSError:
            pass


class SlideView(ModelView):
    def _list_thumbnail(view, context, model, name):
        if not model.filename:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/slides/'+form.thumbgen_filename(model.filename)))

    column_formatters = {
        'filename': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'filename': form.ImageUploadField('Image',
                                      base_path=slide_path,
                                      thumbnail_size=(100, 100, True))
    }

class StoryView(ModelView):
    form_columns = ['photo', 'name', 'link']
    def _list_thumbnail(view, context, model, name):
        if not model.photo:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/stories/' + form.thumbgen_filename(model.photo)))

    column_formatters = {
        'photo': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'photo': form.ImageUploadField('photo',
                                      base_path=story_path,
                                      relative_path = '', # Commented out 'http://localhost:5000/storyimg/' which was giving a filename incorrect error,
                                      thumbnail_size=(100, 100, True))
    }

# class HostView(ModelView):
#     form_columns = ['photo', 'name', 'link']
#     def _list_thumbnail(view, context, model, name):
#         if not model.photo:
#             return ''

#         return Markup('<img src="%s">' % url_for('static',
#                                                  filename='assets/img/stories/' + form.thumbgen_filename(model.photo)))

#     column_formatters = {
#         'photo': _list_thumbnail
#     }

#     # Alternative way to contribute field is to override it completely.
#     # In this case, Flask-Admin won't attempt to merge various parameters for the field.
#     form_extra_fields = {
#         'photo': form.ImageUploadField('photo',
#                                       base_path=story_path,
#                                       relative_path = '', # Commented out 'http://localhost:5000/storyimg/' which was giving a filename incorrect error,
#                                       thumbnail_size=(100, 100, True))
#     }

class AdvertView(ModelView):
    
    def _list_thumbnail(view, context, model, name):
        if not model.filename:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/adverts/'+form.thumbgen_filename(model.filename)))

    column_formatters = {
        'filename': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'filename': form.ImageUploadField('Image',
                                      base_path=advert_path,
                                      thumbnail_size=(100, 100, True))
    }

class ShopView(ModelView):
    def _list_thumbnail(view, context, model, name):
        if not model.filename:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/shop/'+form.thumbgen_filename(model.filename)))

    column_formatters = {
        'filename': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'filename': form.ImageUploadField('Image',
                                      base_path=shop_path,
                                      thumbnail_size=(100, 100, True))
    }

class FlyerView(ModelView):
    def _list_thumbnail(view, context, model, name):
        if not model.filename:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/flyers/'+form.thumbgen_filename(model.filename)))

    column_formatters = {
        'filename': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'filename': form.ImageUploadField('Image',
                                      base_path=flyer_path,
                                      thumbnail_size=(100, 100, True))
    }

# class StoryView(ModelView):
#     form_columns = ['id', 'photo', 'name', 'link', 'lastUpdated']

class ItemView(ModelView):
    
    form_columns = ['type', 'story', 'src', 'length', 'link']
    def _list_thumbnail(view, context, model, name):
        if not model.src:
            return ''

        return Markup('<img src="%s">' % url_for('static',
                                                 filename='assets/img/stories/'+form.thumbgen_filename(model.src)))

    column_formatters = {
        'src': _list_thumbnail
    }

    # Alternative way to contribute field is to override it completely.
    # In this case, Flask-Admin won't attempt to merge various parameters for the field.
    form_extra_fields = {
        'src': form.ImageUploadField('Image',
                                      base_path=story_path,
                                      relative_path = 'http://localhost:5000/storyimg/',
                                      thumbnail_size=(100, 100, True))
        }

# Create customized model view class
class MyModelView(ModelView):
    
    def is_accessible(self):
        return login.current_user.is_authenticated
        
# Create customized index view class that handles login & registration
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    @login.login_required
    @admin_required
    def index(self):
        return super(MyAdminIndexView, self).index()

    @expose('/logout/')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))
    
    


# admin = Admin(app, name='Tubayo Admin', template_mode='bootstrap3')
admin = admin.Admin(app, 'Tubayo Admin', index_view=MyAdminIndexView(), base_template='my_master.html', template_mode='bootstrap3')

# add_view(MyModelView(User, db.session))
admin.add_view(ModelView(User,db.session))
admin.add_view(ModelView(Role,db.session))
admin.add_view(StoryView(Story, db.session))
admin.add_view(ItemView(Item, db.session))
admin.add_view(SlideView(Slideshow, db.session))
admin.add_view(SlideView(SlideshowMobile, db.session))
admin.add_view(ShopView(Shop, db.session))
admin.add_view(AdvertView(Advert, db.session))
admin.add_view(FlyerView(Flyer, db.session))
admin.add_view(ModelView(Imageexp,db.session))
admin.add_view(ModelView(Hosty,db.session))
admin.add_view(ModelView(Book,db.session))
# admin.add_view(ModelView(Rating,db.session))
# admin.add_view(ModelView(Review,db.session))
admin.add_view(ModelView(Comment,db.session))
admin.add_view(ModelView(Date_available,db.session))
