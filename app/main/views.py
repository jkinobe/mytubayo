import os

from flask import render_template, send_from_directory, request, json, jsonify, redirect, url_for, flash
from app import app, db
from app.main import main
# from app.cache import cache
from sqlalchemy.sql import func
from flask_login import current_user, login_required
from app.data.models import Permission, Advert, ShoppingCart, Date_available, Experience, Flyer, Imageexp, Image, Rating, Shop, Slideshow, Story, Item, StorySchema, ItemSchema, User, Review, SlideshowMobile, Category, Hosty, Comment, Book
from flask_mail import Message
from app.email import send_email
from app.form.forms import ExperienceForm, SearchForm, ContactForm, HostForm, CommentForm, BookForm
from app import mail
from app.decorators import admin_required, permission_required
from app.shopping_cart.ShoppingCart import Shopping_Cart
from flask_admin import Admin, form
from app import db
from flask_restful import reqparse
#from flask_security.decorators import roles_required
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.exc import IntegrityError

from werkzeug import secure_filename

#import beyonic

# image routes

# creating thumnailfolder
# thumbnail_path = app.config['RESIZE_ROOT']
# try:
#     os.mkdir(thumbnail_path)
# except OSError:
#     pass

# This snippet is in public domain.
# However, please retain this link in your sources:
# http://flask.pocoo.org/snippets/120/
# Danya Alexeyevsky
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy


from flask import session, redirect, current_app, g
import functools
from datetime import datetime

@main.before_app_request
def before_request():
    # if current_user.is_authenticated:
    #     current_user.last_seen = datetime.utcnow()
    #     db.session.commit()
    g.search_form = SearchForm()

@main.context_processor
def inject_cart_count():
    if current_user.is_anonymous:
        count = 0
    else:
        count = ShoppingCart.query.filter_by(user_id=current_user.id).count()
    return dict(count=count)


def redirect_url(default='main.index'):
    return request.args.get('next') or \
           request.referrer or \
           url_for(default)


# def parse_arg_from_requests(arg, **kwargs):
#     parse = reqparse.RequestParser()
#     parse.add_argument(arg, **kwargs)
#     args = parse.parse_args()
#     return args[arg]

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@main.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['SLIDES'], filename)

@main.route('/experienceimg/<filename>')
def uploaded_experience(filename):
    return send_from_directory(app.config['EXPERIENCES'],filename)

@main.route('/shopimg/<filename>')
def uploaded_item(filename):
    return send_from_directory(app.config['SHOP'],filename)

@main.route('/flyer/<filename>')
def uploaded_flyer(filename):
    return send_from_directory(app.config['FLYERS'],filename)

@main.route('/advert2/<filename>')
def uploaded_advert(filename):
    return send_from_directory(app.config['ADVERTS'],filename)

@main.route('/storyimg/<filename>')
def uploaded_story(filename):
    return send_from_directory(app.config['STORIES'],filename)


@main.route("/message")
def message():

    msg = Message("Hello",
                  sender="from@example.com",
                  recipients=["quinton.ssebaggala@gmail.com"])
    msg.body = "testing"
    msg.html = "<b>testing</b>"

    return mail.send(msg)

@main.route('/')
# @anchor
# @login_required
# @admin_required
# @cache.cached(300, key_prefix='index')
def index():
    slideshow = Slideshow.query.order_by(Slideshow.created_on.desc()).limit(3).all()
    slideshowMobile = SlideshowMobile.query.order_by(SlideshowMobile.created_on.desc()).limit(3).all()
    # stories = Story.query.order_by(Story.created_on.desc()).limit(12).all()
    experience = Imageexp.query.order_by(Imageexp.created_on.desc()).limit(8).all()
    shop = Shop.query.order_by(Shop.created_on.desc()).limit(12).all()
    flyers = Flyer.query.order_by(Flyer.created_on.desc()).limit(2).all()
    adverts = Advert.query.order_by(Advert.created_on.desc()).limit(3).all()
    trend = Imageexp.query.filter_by(trend=True).limit(8).all()
    story = Story.query.all()
    # all_products = Category.query.filter_by(id=category_id).first().experiences
    # cache.delete('detail')
    return render_template('main/main.html', slideshow=slideshow, experience=experience,
        shop=shop, flyers=flyers, adverts=adverts, trend=trend, slideshowMobile=slideshowMobile, story=story)


@main.route('/test')
def test():
    data = Experience.query.order_by(Experience.created_on.desc()).limit(5).all()
    return render_template('test.html', data=data)

@main.route('/shop/')
@main.route('/shop/<int:itemid>', methods = ['GET'])
# @login_required
# @permission_required(Permission.FOLLOW)
def shop(itemid = 0):
    page_num = int(request.args.get('page', 1))
    # shop = Shop.query.order_by(Shop.created_on.desc()).limit(12).all()
    shop = Shop.query.order_by(Shop.created_on.desc()).paginate(page=page_num, per_page=100, error_out=True)
    item = Shop.query.get(itemid)
    return render_template('main/shop.html', shop=shop, item=item)

# @main.context_processor
# def example():
#     return dict(myexample='This is an example')

@main.route('/experience/')
# @anchor
# @login_required
# @permission_required(Permission.FOLLOW)
def experience():
    page_num = request.args.get('page', 1, type=int)
    # shop = Shop.query.order_by(Shop.created_on.desc()).limit(12).all()
    experience = Imageexp.query.order_by(Imageexp.created_on.desc()).paginate(page=page_num, per_page=12, error_out=True)
    return render_template('main/experiences.html', experience=experience)


# @main.route('/add_experience/')
# def add_experience():
#     experience = Experience()
#     return render_template('test.html', data=data)


@main.route('/host/', methods=['GET', 'POST'])
def host():
    host = Hosty()
    form = HostForm()
    if request.method == 'POST':
        # form = ExperienceForm(request.form)
        form.populate_obj(host)
        try:
            db.session.add(host)
            #db.session.commit()
            db.session.commit()
            flash('thank you', 'success')
            return redirect(url_for('main.index'))
            db.session.add(host)
            #db.session.commit()
            db.session.commit()
            
        except:
            session.rollback()
            raise
            flash("Please try again or contact us")
    return render_template('main/host.html', form=form)

@main.route('/becomeahost/', methods=['GET'])
#@roles_required('admin')
def become_a_host():
    return render_template('main/host_landing.html')

@main.route('/help/')
# @anchor
def help():
    return render_template('main/help.html')



@main.route('/detail', methods=['GET'])
# @cache.cached(300, key_prefix='detail')
def detail():
    experience_id = request.args.get('id')
    experience = Experience.query.filter_by(id=experience_id).first()
    comments = Comment.query.filter_by(experience_id=experience_id).all()
    # cache.delete('index')
    return render_template('main/experience_detail.html', experience=experience)


@main.route('/rating', methods=['POST'])
@login_required
@permission_required(Permission.FOLLOW)
def rating():
    # data = request.get_json(force=True)
    # id= parse_arg_from_requests('exp_id')
    exp_id = request.json['exp_id']
    rating = request.json['rating']
    user_id = current_user.id
    rate_count = []
    reviews_count = []

    query = Experience.query.join(Experience.users).filter(
     Experience.id == exp_id,
     User.id == user_id
    )

    query2 = Review.query.filter_by(
     exp_id=exp_id,
     rate=rating
    )

    experience=Experience.query.get_or_404(exp_id)
    user = User.query.get_or_404(user_id)
    vote = Rating.query.filter_by(rating=rating).first()
    review_exit = query2.first()


    try:
        experiences = query.one()
        print('available')
    except MultipleResultsFound as e:
        print (e)
        # Deal with it
    except NoResultFound as e:
        print (e)
        experience.users.append(user)
        if review_exit:
            # review_exit.review_num =+reviews_number
            review_exit.review_num = Review.review_num + 1
        else:
            experience.rates.append(vote)
            review = Review(exp_id=exp_id, rate=rating, review_num=1)
            rate =  Rating.query.filter_by(rating=rating).first()
            rate.reviews.append(review)
        try:
            db.session.commit()
        except IntegrityError as e:
            app.logger.error(e)
            db.session.rollback()

    review_row = Review.query.filter_by(exp_id=exp_id).all()
    # calculating weighted average
    for a in review_row:
        rate_count.append(a.rate)
        reviews_count.append(a.review_num)
    average = sum(x * y for x, y in zip(rate_count, reviews_count)) / sum(reviews_count)
    # updating Weighted_Average column
    experience.Weighted_Average = average
    experience.reviews_no = sum(reviews_count)
    db.session.commit()

    return json.dumps({'status': 'OK', 'rating': rating, 'id': exp_id, 'user': user_id})
    # return redirect( url_for('main.review'))

@main.route('/review', methods=['POST','Get'])
@login_required
#@permission_required(Permission.FOLLOW)
def review():
    rate = request.args.get('rate')
    exp_id = request.args.get('exp_id')
    form = CommentForm()
    comments = Comment.query.filter_by(experience_id=exp_id).all()
    # experience = Experience.query.filter_by(users=exp_id).first()
    # experience = User.query.get(current_user.id).users
    # experience = Experience.query.join(User).filter(User.id == current_user.id, Experience.id == exp_id)
    # experience = Experience.query.filter(Experience.users.any(id=current_user.id)).all()
    # experience = Experience.query.filter(Experience.users == current_user.id).all()
    experience = Experience.query.join(User.users).filter(User.id == current_user.id, Experience.id == exp_id ).all()
    # experience = Experience.query.filter(Experience.users.contains(current_user.id))
    if experience:
        exist = True
    else:
        exist = False

    # return json.dumps({'status': 'OK', 'rating': rating, 'id': exp_id, 'user': user_id})
    return render_template('main/review.html', exp_id=exp_id, rate=rate, exist=exist, form=form, comments=comments)


@main.route('/comment', methods=['POST'])
@login_required
#@permission_required(Permission.FOLLOW)
def comment():
    exp_id = request.args.get('exp_id')
    form = CommentForm(request.form)
    experience = Experience.query.get(exp_id)
    user = User.query.get(current_user.id)
    comment=Comment(comment=form.comment.data)
    user.comment.append(comment)
    experience.exp_comment.append(comment)
    db.session.add(user)
    db.session.add(experience)
    db.session.commit()
    # return redirect(url_for('main.review'))
    return redirect(redirect_url())


    # return json.dumps({'status': 'OK', 'rating': rating, 'id': exp_id, 'user': user_id})
    # return render_template('main/review.html', exp_id=exp_id, rate=rate, exist=exist)




@main.route("/addToCart", methods=['GET'])
@login_required
def addToCart():
    id=request.args.get('exp_id')
    date = request.args.get('date')
    cart = Shopping_Cart(id)
    experience = Experience.query.get(id)
    imageexp = Imageexp.query.filter_by(experience_id=experience.id).first()
    # imageexp.no_people = imageexp.no_people + 1
    # db.session.commit()
    image = Image.query.get(imageexp.image_id)
    cart.add_item(experience.experience_title,1,experience.price,experience.tag,image.filename, date)
    return redirect(url_for('main.Cart'))
    return redirect(redirect_url())
    #####################################################
@main.route("/book", methods=['GET', 'POST'])
@login_required
#@main.route('/host/', methods=['GET', 'POST'])
def bookr():
    #id=request.args.get('exp_id')
    #id = request.args.get('exp_id')
    book = Book()
    form = BookForm()
    experience_title = request.args.get('exp_id')#### here
    if request.method == 'POST':
        # form = ExperienceForm(request.form)
        form.populate_obj(book)
        try:
            
            db.session.add(book)
            #db.session.commit()
            db.session.commit()
            flash('thank you, we shall send in booking details shortly', 'success')
            return redirect(url_for('main.index'))
            db.session.add(book)
            #db.session.commit()
            db.session.commit()
        except Exception as exception:
           # session.rollback()                                              
            current_app.logger.exception(str(exception))
            return dict(message=type(exception).__name__), 400

    return render_template('main/book.html', form=form)

    ###########################################################
@main.route("/cart/", methods=['GET', 'POST'])
@login_required
def Cart():
    try:
    #if user not in session:
    #    return redirect(url_for('main.index'))
    #else:
        item = ShoppingCart.query.filter_by(user_id=current_user.id).all()
        total2 = ShoppingCart.query.with_entities(func.sum(ShoppingCart.total)).all()
        count = ShoppingCart.query.filter_by(user_id=current_user.id).count()
        print(item)
    except:
        return redirect(url_for('main.index'))

    # review_exit.review_num = Review.review_num + 1
    if request.method == 'POST':
        for y, c in zip(request.form, item):
            quantity = int(request.form[y])
            total = int()

            kart = ShoppingCart.query.filter_by(id=c.id).first()
            total += (quantity * kart.price)
            kart.quantity = quantity
            kart.total = total
            try:
                db.session.commit()
            except IntegrityError as e:
                app.logger.error(e)
                db.session.rollback()
        return redirect(url_for('main.Cart'))
    return render_template('main/shoppingcart.html', item=item, total2=total2, count=count)


@main.route("/remove_item", methods=['GET'])
@login_required
def remove_item():
    id=request.args.get('cartitem_id')
    item = ShoppingCart.query.get(id)
    try:
        db.session.delete(item)
        db.session.commit()
    except IntegrityError as e:
        app.logger.error(e)
        db.session.rollback()
    return redirect(url_for('main.Cart'))

@main.route('/checkout/')
# @anchor
def checkout():
    # send_email('[Tubayo Travel] Reset Your Password',
    #            sender=app.config['TUBAYO_ADMIN'][0],
    #            recipients=[user.email],
    #            text_body=render_template('email/host.txt',
    #                                      user=user, token=token),
    #            html_body=render_template('email/reset_password.html',
    #                                      user=user, token=token))
    return render_template('main/checkout.html')


@main.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('Expeirence.shop'))
    page = request.args.get('page', 1, type=int)
    experience, total = Experience.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    imageexp = Imageexp.query.order_by(Imageexp.created_on.desc()).all()
    return render_template('main/search.html', title='Search', experience=experience, total=total,
                           next_url=next_url, prev_url=prev_url, imageexp=imageexp)

@main.route("/contact/", methods=['GET', 'POST'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        # user = User(username=form.username.data, email=form.email.data)
        recipients=['tubayotravel@gmail.com'],
        text_body=form.email.data+ '\n' +form.body.data,
        html_body='<h3>Thanks for registering with Tubayo travel!</h3>'
        # send_email(subject=form.subject.data,
        #        sender=form.email.data,
        #        recipients=app.config['TUBAYO_ADMIN'],
        #        text_body=form.body.data,
        #        html_body='')
        flash('message sent', 'success')
        redirect(url_for('main.contact'))

    return render_template('main/contact_us.html', form=form)

@main.route("/soon/", methods=['GET', 'POST'])
def soon():

    return render_template('main/soon.html')

@main.route("/success/", methods=['GET', 'POST'])
def payment():
    beyonic.api_key = app.config['BEYONIC_KEY']

    collection_request = beyonic.CollectionRequest.create(phonenumber="+80000000001",
        amount="1200",
        currency="BXC",
        description="Per diem",
        callback_url="https://requestbin.fullcontact.com/zsa20pzs",
        #metadata={"my_id": "123ASDAsd123"},
        send_instructions = True
    )

    print (collection_request)

    flash('Your payment was successfully registered')
    return redirect(url_for('main.index'))
