from app.data.models import Experience, ShoppingCart, User, Imageexp
from sqlalchemy.exc import IntegrityError
from app import app, db
from flask_login import current_user
from sqlalchemy.orm.exc import NoResultFound

class Shopping_Cart():
    def __init__(self, id):
        self.id =id
        self.total = 0
        self.experience = Experience.query.get(self.id)
        self.user = User.query.get(current_user.id)

    def add_item(self, item_name, quantity, price, description, filename, date):
        
        try:

            cart = ShoppingCart.query.filter_by(experience_id=self.experience.id, user_id=self.user.id, date=date).one()

        except NoResultFound as e:

            app.logger.error(e)
            self.total += (quantity * price)
            kart = ShoppingCart(item_name=item_name, price=price, quantity=quantity, total=self.total, description=description, filename=filename, date=date)
            self.experience.carts.append(kart)
            self.user.carts.append(kart)

            imageexp = Imageexp.query.filter_by(experience_id=self.experience.id).first()
            imageexp.no_people = imageexp.no_people + 1 

            db.session.add(self.experience)
            db.session.add(self.user)
            try:
                db.session.commit()
            except IntegrityError as e:
                app.logger.error(e)
                db.session.rollback()
